## Knipster test task

### Server requirements
- PHP >= 7.0
- OpenSSL PHP Extension
- PDO PHP Extension
- PDO SQLITE PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- [Composer](https://getcomposer.org/)

### Installation

Follow the steps below for deploy:

- Run in console `git clone git@bitbucket.org:boowee/knipster-test.git` for checkout project
- `cd knipster-test`
- Run in console `composer install` for install requirements
- Create database
- Create `.env` file and set environment variables, use for example `.env.example`
- Run in console `php artisan key:generate` for generate project salt key
- Start php web server `php artisan serve`
- Create database structure `php artisan migrate`
- Fill database `php artisan db:seed`

### Tests
Run command in root folder your project: `vendor/bin/phpunit`

### Calls api
- get request `/api/customers` - list customers
- get request `/api/customers/{customerId}` - get customer info
- post request `/api/customers` - store customer by given data, parameters {email: required, first_name: required, gender: required}
- put request `/api/customers/{customerId}` - update customer by given data

- post request `/api/customers/{customerId}/deposit` - execute deposit operation, parameters {amount: required}
- post request `/api/customers/{customerId}/withdraw` - execute withdraw operation, parameters {amount: required}
- get request `/reporting` - get total deposits and withdrawals report