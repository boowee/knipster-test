<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOperation;
use App\Operation;
use App\Customer;
use App\Repositories\OperationRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OperationsController extends Controller
{
    public function deposit(Customer $customer, StoreOperation $request)
    {
        $operation = Operation::execute($request->input('amount'), Operation::TYPE_DEPOSIT, $customer);

        return response()->json($operation->customer, 201);
    }

    public function withdraw(Customer $customer, StoreOperation $request)
    {
        $operation = Operation::execute($request->input('amount'), Operation::TYPE_WITHDRAW, $customer);

        return response()->json($operation->customer, 201);
    }

    public function reporting(OperationRepository $operationRepository, Request $request)
    {
        $dtFrom = \DateTime::createFromFormat('Y-m-d', $request->input('from'));
        $dtTo = \DateTime::createFromFormat('Y-m-d', $request->input('to'));

        $from = $dtFrom ? Carbon::instance($dtFrom) : null;
        $to = $dtTo ? Carbon::instance($dtTo) : null;

        return response()->json($operationRepository->getReportPerCountryDate($from, $to));
    }
}
