<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomer;
use App\Http\Requests\UpdateCustomer;
use App\Customer;

class CustomersController extends Controller
{
    public function index()
    {
        return  response()->json(Customer::all());
    }

    public function show(Customer $customer)
    {
        return response()->json($customer);
    }

    public function store(StoreCustomer $request)
    {
        $customer = Customer::create($request->only(["first_name", "last_name", "country", "email", "gender"]));

        return response()->json($customer, 201);
    }

    public function update(UpdateCustomer $request, Customer $customer)
    {
        $customer->update($request->all());

        return response()->json($customer);
    }

    public function delete(Customer $customer)
    {
        $customer->delete();

        return response()->json(null, 204);
    }
}
