<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;

class Customer extends Model
{

    protected $fillable = [
        "first_name",
        "last_name",
        "country",
        "email",
        "gender"
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->bonus = $model->bonus ?: rand(5,20);
        });
    }

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }

    public function getCountOperations($type = Operation::TYPE_DEPOSIT)
    {
        return $this->operations()->where('type', $type)->count();
    }

    public function processingOperation(Operation $operation)
    {
        $customer = static::where('id', $this->id)->lockForUpdate()->first();

        if($operation->type == Operation::TYPE_DEPOSIT) {
            $customer->real_balance += $operation->amount;
            $this->applyingBonus ($customer, $operation);
        }
        elseif ($operation->type == Operation::TYPE_WITHDRAW) {
            $customer->real_balance -= $operation->amount;
        }

        if($customer->real_balance < 0) {
            throw new HttpException( Response::HTTP_LOCKED, 'Balance below the amount requested');
        }

        $customer->save();

        return $customer;
    }


    protected function applyingBonus($customer, $operation)
    {
        if($customer->getCountOperations() % 3 == 0) {
            $customer->bonus_balance += $operation->amount * $customer->bonus / 100;
        }
    }
}
