<?php

namespace App\Repositories;

use App\Operation;
use DB;
use Carbon\Carbon;

class OperationRepository
{
    protected $model;

    public function __construct(Operation $model)
    {
        $this->model = $model;
    }

    public function getReportPerCountryDate(Carbon $from = null, Carbon $to = null)
    {
        $typeDeposit = Operation::TYPE_DEPOSIT;
        $typeWithdraw = Operation::TYPE_WITHDRAW;

        if(!$from) {
            $from = Carbon::now()->subDays(6);
        }

        $report = DB::table('operations')
            ->join('customers', 'customers.id', '=', 'operations.customer_id')
            ->select(
                DB::raw("DATE(`operations`.created_at) `date`,
                    `customers`.country,
                    COUNT(DISTINCT  `customers`.id) customers_count,
                    SUM( IF (`operations`.`type` = $typeDeposit, 1, 0)) number_deposit,
                    SUM( IF (`operations`.`type` = $typeDeposit, `operations`.amount, 0)) total_deposit,
                    SUM( IF (`operations`.`type` = $typeWithdraw, 1, 0)) number_withdraw,
                    SUM( IF (`operations`.`type` = $typeWithdraw, `operations`.amount, 0)) total_withdraw"))
            ->whereRaw(sprintf("DATE(`operations`.created_at) >= '%s'", $from->format('Y-m-d')))
            ->groupBy(DB::raw("`customers`.country, DATE(`operations`.created_at)"))
            ->orderBy('date');

        if($to) {
            $report->whereRaw(sprintf("DATE(`operations`.created_at) <= '%s'", $to->format('Y-m-d')));
        }

        return $report->get();
    }
}