<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    const TYPE_DEPOSIT = 1;
    const TYPE_WITHDRAW = 2;

    protected $fillable = [
        "type",
        "amount",
        "customer_id",
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public static function execute($amount, $type, $customer)
    {
        return \DB::transaction(function () use ($amount, $type, $customer){

            $operation = static::create([
                'amount' => $amount,
                'type' => $type,
                'customer_id' => $customer->id
            ]);

            $customer->processingOperation($operation)->fresh();

            return $operation;
        });
    }

}
