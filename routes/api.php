<?php

use Illuminate\Http\Request;
use App\Customer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('customers', 'CustomersController@index');
Route::get('customers/{customer}', 'CustomersController@show');
Route::post('customers', 'CustomersController@store');
Route::put('customers/{customer}', 'CustomersController@update');

Route::post('customers/{customer}/deposit', 'OperationsController@deposit');
Route::post('customers/{customer}/withdraw', 'OperationsController@withdraw');
Route::get('reporting', 'OperationsController@reporting');
