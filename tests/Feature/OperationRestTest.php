<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Customer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OperationRestTest extends TestCase
{
    use DatabaseTransactions;

    public function testDeposit()
    {
        $customer = factory(Customer::class)->create();

        $this->json('POST', "/api/customers/{$customer->id}/deposit")
            ->assertStatus(422);

        $amount = 50;
        $realBalance = $amount;
        $this->json('POST', "/api/customers/{$customer->id}/deposit", ['amount' => $amount])
            ->assertStatus(201)
            ->assertJson(['real_balance' => $realBalance]);

        $amount = 100;
        $realBalance += $amount;
        $this->json('POST', "/api/customers/{$customer->id}/deposit", ['amount' => $amount])
            ->assertStatus(201)
            ->assertJson(['real_balance' => $realBalance]);

        $amount = 150;
        $realBalance += $amount;
        $bonus = $amount * $customer->bonus / 100;

        $this->json('POST', "/api/customers/{$customer->id}/deposit", ['amount' => $amount])
            ->assertStatus(201)
            ->assertJson(['real_balance' => $realBalance, 'bonus_balance' => $bonus]);
    }

    public function testWithdraw()
    {
        $customer = factory(Customer::class)->create();

        $this->json('POST', "/api/customers/{$customer->id}/withdraw")
            ->assertStatus(422);

        $this->json('POST', "/api/customers/{$customer->id}/deposit", ['amount' => 20]);

        $this->json('POST', "/api/customers/{$customer->id}/withdraw", ['amount' => 22])
            ->assertStatus(423);

        $this->json('POST', "/api/customers/{$customer->id}/withdraw", ['amount' => 12.5])
            ->assertStatus(201)
            ->assertJson(['real_balance' => 7.5]);

    }
}
