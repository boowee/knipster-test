<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Customer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomerRestTest extends TestCase
{
    use DatabaseTransactions;

    public function testCustomerLists()
    {
        $this->json('get', '/api/customers')
            ->assertStatus(200)
            ->assertJsonStructure([
                 '*'=> [ 'id', 'email', 'country', 'bonus', 'first_name', 'gender', 'last_name'],
            ]);
    }

    public function testCustomerCreate()
    {
        $customer = factory(Customer::class)->create();

        $this->json('post', '/api/customers', [
            'email' => $customer->email,
            'first_name' => 'John',
            'gender' => 1,
            'country' => 'US',
        ])->assertStatus(422);

        $customer->delete();

        $this->json('post', '/api/customers', [
            'email' => 'test@test.com',
            'first_name' => 'John',
            'gender' => 1,
            'country' => 'US',
        ])
            ->assertStatus(201)
            ->assertJsonStructure([
                'id', 'email', 'country', 'bonus', 'first_name', 'gender'
            ]);
    }

    public function testCustomerUpdate()
    {
        $customer = factory(Customer::class)->create();

        $this->json('put', "/api/customers/{$customer->id}", [
            'first_name' => 'Ivan',
            'country' => 'RU',
        ])
            ->assertStatus(200)
            ->assertJson([
                'first_name' => 'Ivan', 'country' => 'RU'
            ]);
    }
}
