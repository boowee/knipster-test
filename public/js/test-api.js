+function ($) { "use strict";

    var TestApi = function () {};

    TestApi.prototype.customersList = function() {

        $.ajax('/api/customers',{
            method:'get'
        }).done(function(response) {
                console.log(response);
        });

    };

    TestApi.prototype.customerCreate = function() {

        $.ajax('/api/customers',{
            method:'post',
            data: {
                first_name: "John",
                // last_name: "Doe",
                country: "US",
                email: "john.done@yahoo.com",
                gender: "1"

            }
        }).done(function(response) {
                console.log(response);
            });
    };

    TestApi.prototype.customerUpdate = function() {

        $.ajax('/api/customers/1',{
            method:'put',
            data: {
                country: "EN",
                email: "john.done@gmail.com"
            }
        }).done(function(response) {
            console.log(response);
        });
    };


    TestApi.prototype.customerDelete = function() {

        $.ajax('/api/customers/1',{
            method:'delete'
        }).done(function(response) {
            console.log(response);
        });
    };

    TestApi.prototype.deposit = function(customerId = 4, amount = 10.10) {

        $.ajax(`/api/customers/${customerId}/deposit`,{
            method:'post',
            data: {amount: amount}
        }).done(function(response) {
            console.log(response);
        });
    };

    TestApi.prototype.withdraw = function(customerId = 1, amount = 11.11) {

        $.ajax(`/api/customers/${customerId}/withdraw`,{
            method:'post',
            data: {amount: amount}
        }).done(function(response) {
            console.log(response);
        });
    };


    TestApi.prototype.integrityDeposit = function() {
        this.deposit();
        this.deposit();
        this.deposit();
    };


    TestApi.prototype.reporting = function() {
        $.ajax('/api/reporting/',{
            method:'get',
            // data: {from: '2017-11-10', to: '2017-11-11'}
        }).done(function(response) {
                console.log(response);
        });
    };

    $.testApi = new TestApi();

}(window.jQuery);