<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {

            $table->increments('id');
            $table->tinyInteger('type');
            $table->unsignedDecimal('amount', 10, 2);
            $table->unsignedInteger('customer_id');
            $table->timestamps();
//            $table->unique(['type', 'amount', 'customer_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
