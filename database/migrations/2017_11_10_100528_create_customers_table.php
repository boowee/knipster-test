<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->tinyInteger('gender');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('country')->nullable();
            $table->unsignedMediumInteger('bonus')->default(0);
            $table->unsignedDecimal('real_balance', 10, 2)->default(0);
            $table->unsignedDecimal('bonus_balance', 10, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
