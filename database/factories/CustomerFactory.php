<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Customer::class, function (Faker $faker) {
    $gender = rand(1, 2);
    return [
        'email' => $faker->unique()->safeEmail,
        'gender' => rand(1,2),
        'first_name' => $gender == 1 ? $faker->firstNameMale : $faker->firstNameFemale,
        'last_name' => $faker->lastName,
        'country' => $faker->countryCode
    ];
});
