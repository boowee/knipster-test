<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < rand(11,20); $i++) {

            $gender = rand(1, 2);

            Customer::create([
                'email' => $faker->email,
                'gender' => $gender,
                'first_name' => $gender == 1 ? $faker->firstNameMale : $faker->firstNameFemale,
                'last_name' => $faker->lastName,
                'country' => $faker->countryCode
            ]);
        }
    }
}
